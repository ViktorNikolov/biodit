package com.biodit.task.model;

import com.biodit.task.config.HibernateConfig;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

public class Importer {


    public static void main(String[] args) {
        importer();
        System.out.println("Done");
    }

    public static void create(Employee employee) {
        SessionFactory sessionFactory = HibernateConfig.buildSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            session.save(employee);
        }
    }

    public static void importer() {

        File file = new File("C:\\Grafik.xlsx");
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
            XSSFSheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext()) {
                Employee employee = new Employee();
                Row nextRow = rowIterator.next();
                Iterator<Cell> cellIterator = nextRow.cellIterator();

                while (cellIterator.hasNext()) {
                    Cell nextCell = cellIterator.next();
                    switch (nextCell.getColumnIndex()) {
                        case 0:
                            employee.setId(nextCell.getRowIndex());
                            break;

                        case 1:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                case NUMERIC:
                                    break;
                                case STRING:
                                    employee.setWorkPlace(nextCell.getStringCellValue());
                                    break;
                            }
                            break;


                        case 2:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case NUMERIC:
                                    employee.setCodeEmployee((int) nextCell.getNumericCellValue());
                                    break;
                            }
                            break;

                        case 3:
                            employee.setNameEmployee(nextCell.getStringCellValue());
                            break;

                        case 4:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM1(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 5:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM2(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 6:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM3(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 7:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM4(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 8:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM5(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 9:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM6(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 10:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM7(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 11:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM8(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 12:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM9(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 13:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM10(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 14:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM11(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 15:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM12(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 16:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM13(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 17:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM14(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 18:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM15(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 19:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM16(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 20:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM17(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 21:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM18(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 22:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM19(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 23:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM20(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 24:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM21(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 25:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM22(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 26:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM23(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 27:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM24(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 28:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM25(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 29:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM26(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 30:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM27(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 31:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM28(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 32:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM29(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 33:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM30(nextCell.getStringCellValue());
                                    break;
                            }
                            break;

                        case 34:
                            switch (nextCell.getCellType()) {
                                case BLANK:
                                    break;
                                case STRING:
                                    employee.setM31(nextCell.getStringCellValue());
                                    break;
                            }
                            break;
                    }
                }

                create(employee);
            }

        } catch (IOException e) {
            throw new RuntimeException("Fail to parse Excel file: " + e.getMessage());
        }
    }


}
