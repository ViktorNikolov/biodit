package com.biodit.task.config;

import com.biodit.task.model.Employee;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;
import org.w3c.dom.Document;

import java.util.Properties;


public class HibernateConfig {

    public static SessionFactory buildSessionFactory() {
        try {

            Properties properties = new Properties();
            properties.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
            properties.put(Environment.URL, "jdbc:mysql://localhost:3306/biodit?useUnicode=true&characterEncoding=UTF-8");
            properties.put(Environment.USER, "Biodit");
            properties.put(Environment.PASS, "Biodit");
//            properties.put(Environment.FORMAT_SQL, "true");
            properties.put(Environment.DIALECT, "org.hibernate.dialect.MySQLDialect");
//            properties.put(Environment.SHOW_SQL, "true");
//            properties.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
//            properties.put(Environment.HBM2DDL_AUTO, "update");
//            properties.put(Environment.POOL_SIZE, "5");

            return new Configuration()
                    .setProperties(properties)
                    .addAnnotatedClass(Employee.class)
                    .buildSessionFactory();

        } catch (Throwable ex) {
            System.err.println("Build SessionFactory failed :" + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
}
